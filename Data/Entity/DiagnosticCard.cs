﻿namespace Data
{
    public class DiagnosticCard : BaseEntity
    {
        public Operator Operator { get; set; }

        public City City { get; set; }
       
        public string Number { get; set; }

        public string Client { get; set; }

    }
}