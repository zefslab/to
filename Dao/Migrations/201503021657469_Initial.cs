namespace Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DiagnosticCard",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        number = c.String(nullable: false),
                        client = c.String(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        modified_date = c.DateTime(nullable: false),
                        city_id = c.Int(nullable: false),
                        operator_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.City", t => t.city_id, cascadeDelete: true)
                .ForeignKey("dbo.Operator", t => t.operator_id, cascadeDelete: true)
                .Index(t => t.city_id)
                .Index(t => t.operator_id);
            
            CreateTable(
                "dbo.City",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        created_date = c.DateTime(nullable: false),
                        modified_date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Operator",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        created_date = c.DateTime(nullable: false),
                        modified_date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DiagnosticCard", "operator_id", "dbo.Operator");
            DropForeignKey("dbo.DiagnosticCard", "city_id", "dbo.City");
            DropIndex("dbo.DiagnosticCard", new[] { "operator_id" });
            DropIndex("dbo.DiagnosticCard", new[] { "city_id" });
            DropTable("dbo.Operator");
            DropTable("dbo.City");
            DropTable("dbo.DiagnosticCard");
        }
    }
}
