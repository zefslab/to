﻿using System.Data.Entity.ModelConfiguration;
using Data;

namespace Dao.EFConfigurations
{
    public abstract class BaseEntityConfiguration<TEntity> : EntityTypeConfiguration<TEntity>
        where TEntity : BaseEntity
    {
        protected BaseEntityConfiguration()
        {
            Property(u => u.Id).HasColumnName("id");
            Property(u => u.CreatedDate).HasColumnName("created_date");
            Property(u => u.ModifiedDate).HasColumnName("modified_date");
        }
    }
}
