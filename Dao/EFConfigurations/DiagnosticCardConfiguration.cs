﻿using Data;

namespace Dao.EFConfigurations
{
    public class DiagnosticCardConfiguration : BaseEntityConfiguration<DiagnosticCard>
    {
        public  DiagnosticCardConfiguration ()
        {
            Property(x => x.Number).HasColumnName("number").IsRequired();
            Property(x => x.Client).HasColumnName("client").IsRequired();
            HasRequired(x=> x.City).WithMany().Map(x=> x.MapKey("city_id"));
            HasRequired(x => x.Operator).WithMany().Map(x => x.MapKey("operator_id"));

            ToTable("DiagnosticCard");
        }
    }
}
