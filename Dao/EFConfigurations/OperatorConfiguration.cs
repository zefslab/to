﻿using Data;

namespace Dao.EFConfigurations
{
    public class OperatorConfiguration : BaseEntityConfiguration<Operator>
    {
        public OperatorConfiguration()
        {
            Property(x => x.Name).HasColumnName("name");

            ToTable("Operator");
        }
    }
}
