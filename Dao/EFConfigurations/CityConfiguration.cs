﻿using Data;

namespace Dao.EFConfigurations
{
    public class CityConfiguration : BaseEntityConfiguration<City>
    {
        public CityConfiguration()
        {
            Property(x => x.Name).HasColumnName("name");

            ToTable("City");
        }
    }
}
