﻿using Dao.EFConfigurations;
using System.Data.Entity;

namespace Dao
{
    public class DbConnection : DbContext
    {
        public DbConnection()
        {
           
        }

        public virtual DbSet<TEntity> GetSet<TEntity>() where TEntity : class
        {
            return Set<TEntity>();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new DiagnosticCardConfiguration());
            modelBuilder.Configurations.Add(new CityConfiguration());
            modelBuilder.Configurations.Add(new OperatorConfiguration());
        }
    }
}
